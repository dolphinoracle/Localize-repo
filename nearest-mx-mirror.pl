#!/usr/bin/perl

use strict;
use warnings;
use Data::Dumper;
use Math::Trig qw/great_circle_distance pi/;

my $zone_file = "zone.tab";

my $IGNORE_DEB_TZ = "Canary|Ceuta";

my @DEB_CODES =  qw/am au at by be br bg ca cl cn hr cz dk sv ee
    fi fr de gr hk hu is it jp kr lt mx md nc nl no nz pl pt
    ro ru sg sk si es se ch tw tr uk us/;

my @CITIES;
my $CCODES;
my $BY_TZ;

my $DEB_EXCEPTIONS = {
    "Asia/Almaty" => "ru"
};

my $MX_TZ = {

    AU1 => "Australia/Melbourne",
    AU2 => "Australia/Hobart",
    AT => "Europe/Vienna",
    BE => "Europe/Brussels",
    BG => "Europe/Sofia",
    BR => "America/Sao_Paulo",
    CA => "America/Halifax",
    CH => "Europe/Zurich",
    CN => "Asia/Shanghai",
    CZ => "Europe/Prague",
    DE => "Europe/Berlin",
    DK => "Europe/Copenhagen",
    EC => "America/Guayaquil",
    ES => "Europe/Madrid",
    FR => "Europe/Paris",
    GB => "Europe/London",
    GE => "Asia/Tbilisi",
    GR => "Europe/Athens",
    HK => "Asia/Hong_Kong",
    HU => "Europe/Budapest",
    ID => "Asia/Jakarta", 
    IN => "Asia/Kolkata",
    IT => "Europe/Rome",
    JP => "Asia/Tokyo",
    KE => "Africa/Nairobi",
    KZ => "Asia/Almaty",
    LA => "America/Los_Angeles",
    LT => "Europe/Vilnius", 
    MY => "Asia/Kuala_Lumpur",
    NL => "Europe/Amsterdam",
    NY => "America/New_York",
    NZ => "Australia/Brisbane",
    PH => "Asia/Manila",
    PK => "Asia/Karachi",
    PL => "Europe/Warsaw",
    RU => "Europe/Moscow",
    SE => "Europe/Stockholm",
    SG => "Asia/Singapore",
    SI => "Europe/Ljubljana",
    SK => "Europe/Bratislava",
    TH => "Asia/Bangkok",
    TR => "Europe/Istanbul", 
    TW => "Asia/Taipei",
    UA => "Europe/Kiev",
    UT => "America/Denver",
    UY => "America/Montevideo",
    VN => "Asia/Ho_Chi_Minh",  
    ZA => "Africa/Johannesburg",
    
};

my @MX_CITIES;

#my $DEB_CITIES;

my $NEAREST;

read_zone_file($zone_file);
my @DEB_CITIES;
for my $deb (@DEB_CODES) {
    exists $CCODES->{$deb} or die "Missing code for deb country $deb";
    push @DEB_CITIES, grep {$_->{tz} !~ /$IGNORE_DEB_TZ/} @{$CCODES->{$deb}};

    #$DEB_CITIES->{$deb} = $CCODES->{$deb};
}

#print Dumper \@CITIES; exit;
#
for my $mx (sort keys %$MX_TZ) {
    #print "$mx $MX_TZ->{$mx}\n";
    my $tz = $MX_TZ->{$mx};
    my $entry = { %{$BY_TZ->{$tz}} } or die "No entry for timezone $tz";
    $entry->{mx} = $mx;
    #print Dumper $entry;
    push @MX_CITIES, $entry;
}

# print Dumper @DEB_CITIES; exit;

for my $city (@CITIES) {
    my $min = 1000000;
    my $near = undef;
    my $region = $city->{region};

    my $mx_dist = [];
    for my $mx (@MX_CITIES) {
        push @$mx_dist, { mx => $mx->{mx}, dist => my_distance($city, $mx) };
    }

    $city->{mx_dist} = [ sort {$a->{dist} <=> $b->{dist}} @$mx_dist ];
    
    #print Dumper $city;
    #exit;

    for my $deb (@DEB_CITIES) {
        $deb->{region} eq $region
            or $region eq "Indian" or $region eq "Arctic" 
            or $region eq "Africa" or next;
        my $dist = my_distance( $city, $deb);
        #printf "%30s %4.2f\n", $deb->{tz}, $dist;
        $dist >= $min and next;
        $min = $dist;
        $near = $deb;
    }
    $city->{deb} = $near->{code};
    my $deb_exception = $DEB_EXCEPTIONS->{$city->{tz}};
    $deb_exception and $city->{deb} = $deb_exception;

    #print "$near->{code}\n";
    #
    #printf "%4.2f %-30s %-30s %8.2f %8.2f %8.2f %8.2f\n", 
    #    $min, $city->{tz}, $near->{tz},
    #    $city->{latd}, $city->{lond},
    #    $near->{latd}, $near->{lond};

    #push @{$NEAREST->{$near->{code}}}, $city->{code};
    #die;
}

for my $city (sort { $a->{region} cmp $b->{region} || $a->{lon} <=> $b->{lon} } @CITIES) {
    my $mx_code = join ",", map { $_->{mx} } @{$city->{mx_dist}};

    printf "%35s) deb_code=%s ; mx_order=%s ;;\n", $city->{tz}, $city->{deb}, lc($mx_code);
}


exit;
for my $near (sort keys %$NEAREST) {
    #my @codes = grep  $_ ne $near, unique(@{$NEAREST->{$near}});
    my @codes = unique(@{$NEAREST->{$near}});
    @codes or next;
    while (@codes) {
        my @first = splice @codes, 0, 12;
        
        printf "%43s) ccode=%s;;\n", join("|", @first), $near;
    }
    #my $codes = join "|", @codes;
    #print "$near: @codes\n";
}
#print Dumper( $CCODES) ;

#================================================================================
sub read_zone_file {
    my $fname = shift;
    open my $file, "<", $fname || die "Could not open($fname) $!\n";

    while (<$file>) {
        my $entry = line_to_entry($_);
        $entry or next;
        push @CITIES, $entry;
        my $code = $entry->{code};
        push @{$CCODES->{$code}}, $entry;
        $BY_TZ->{$entry->{tz}} = $entry;
    }
}

sub line_to_entry {
    my $line = shift;

    $line =~ m/^(\w{2})\t([+-]\d+)([+-]\d+)\t([\w\/]+)/ or do {
        #print;
        return;
    };

    return city_entry($1, $2, $3, $4);
}

sub city_entry {
    my ($code, $lat, $lon, $tz) = @_;

    my $lat_len = length $lat;
    my $lon_len = length $lon;

    if ($lat_len == 5) {
        $lat /= 100;
    }
    elsif ($lat_len ==7) {
        $lat /= 10000;
    }
    else {
        die "Bad lat: $lat @ $tz"
    }

    if ($lon_len == 6) {
        $lon /= 100;
    }
    elsif ($lon_len == 8) {
        $lon /= 10000;
    }
    else {
        die "Bad lon: $lon @ $tz"
    }

    $code =~ tr/A-Z/a-z/;
    $code eq "gb" and $code = "uk";

    my $region = $tz;
    $region =~ s{/.*}{};

    #print "$code $tz: $lat  $lon\n";
    my $entry = {
        code => $code,
        lat  => pi() / 2 - $lat * pi() / 180,
        lon  => $lon * pi() / 180,
        latd => $lat,
        lond => $lon,
        tz   => $tz,
        region => $region,
    };

    return $entry;
}

sub my_distance {
    my ($c1, $c2) = @_;
    #print "$c1->{lon}:$c1->{lat}  $c2->{lon}:$c2->{lat}\n";
    return great_circle_distance(
        $c1->{lon},
        $c1->{lat}, 
        $c2->{lon},
        $c2->{lat},
    );
}

sub unique {
    my %hash;
    my @result;
    for (@_) {
        $hash{$_}++ and next;
        push @result, $_,
    }
    return @result;
}
